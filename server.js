require("dotenv").config();

const express = require("express");
const cors = require("cors");
const app = express();
app.use(cors());
const mongoose = require("mongoose");

mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log.error(error));
db.once("open", () => console.log("Connected to Database"));

app.use(express.json());

const favoritesRouter = require("./routes/favorites");
app.use("/favorites", favoritesRouter);
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log("Server started", PORT));
