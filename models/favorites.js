const mongoose = require("mongoose");

const favoriteSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
  },
  title: {
    type: String,
    required: true,
  },
  authors: {
    type: Array,
    required: true,
  },
  categories: {
    type: Array,
    required: true,
  },
  publisher: {
    type: String,
    required: false,
  },
  language: {
    type: String,
    required: false,
  },
  averageRating: {
    type: Number,
    required: false,
  },
  ratingsCount: {
    type: Number,
    required: false,
  },
  imageLink: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model("Favorite", favoriteSchema);
