const express = require("express");
const router = express.Router();
const Favorite = require("../models/favorites");

// getting all
router.get("/", async (req, res) => {
  try {
    const favorites = await Favorite.find();
    res.json(favorites);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// getting one
router.get("/:id", getFavorite, (req, res) => {
  res.send(res.favorite);
});

// creating one
router.post("/", async (req, res) => {
  const getFav = await Favorite.findOne({ id: req.body.id });

  // if(res.favorite) return res.status(400).json({ message: 'favorite list with same Id already on DB' })
  if (getFav)
    return res
      .status(400)
      .json({ message: "favorite list with same Id already on DB" });

  const favorite = new Favorite(req.body);

  try {
    const newFavorite = await favorite.save();
    res.status(201).json(newFavorite);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// updating one
router.patch("/:id", getFavorite, async (req, res) => {
  if (req.body.id != null) {
    res.favorite.id = req.body.id;
  }
  if (req.body.title != null) {
    res.favorite.title = req.body.title;
  }
  if (req.body.authors != null) {
    res.favorite.authors = req.body.authors;
  }
  if (req.body.categories != null) {
    res.favorite.categories = req.body.categories;
  }
  if (req.body.publisher != null) {
    res.favorite.publisher = req.body.publisher;
  }
  if (req.body.language != null) {
    res.favorite.language = req.body.language;
  }
  if (req.body.averageRatin != null) {
    res.favorite.averageRatin = req.body.averageRatin;
  }
  if (req.body.ratingsCount != null) {
    res.favorite.ratingsCount = req.body.ratingsCount;
  }
  if (req.body.imageLink != null) {
    res.favorite.imageLink = req.body.imageLink;
  }
  try {
    const updateFavorite = await res.favorite.save();
    res.json(updateFavorite);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// delete one
router.delete("/:id", getFavorite, async (req, res) => {
  try {
    await res.favorite.remove();
    res.json({ message: "Deleted favorite" });
  } catch {
    res.status(500).json({ message: err.message });
  }
});

async function getFavorite(req, res, next) {
  let favorite;
  let id = req.params.id ? req.params.id : req.body.id;
  try {
    favorite = await Favorite.findOne({ id: id });
    if (favorite == null) {
      return res.status(404).json({ message: "Cannot find favorite" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.favorite = favorite;
  next();
}

module.exports = router;
